<html lang="ru">
<head>
    <title>Ханойская башня</title>
    <style>.layer {
            padding-top: 10px;
        }</style>
</head>
<body>
<div>
    <h1>Ханойская башня</h1>
    <h3>Условие задачи:</h3>
    <span> Даны три стержня, на один из которых нанизаны от трёх до восьми колец, кольца отличаются размером и лежат меньшее на большем.<br>
Задача состоит в том, чтобы перенести башню-пирамиду за наименьшее число ходов на другой стержень.<br>
За один раз разрешается переносить только одно кольцо, причём нельзя класть большее кольцо на меньшее.</span>
</div>
<img src="https://alexandrsoldatkin.com/wp-content/uploads/2016/10/towershanoi.jpg" alt="">
<div class="layer">
    <form method="post">
        <label for="name">Введите количество дисков:</label>
        <input type="text" name="num" value="" placeholder="0т 3 до 8"/>
        <input type="submit" name="submit" value="Submit"/>
    </form>
</div>
<?php
$step = 0;
function towerOfHanoi($n, $a, $b, $c)
{
    global $step;
    if ($n == 1) {
        $step++;
        echo "Диск $n с стержня $a на стержень $b <br />";
    } else {
        towerOfHanoi($n - 1, $a, $c, $b);
        $step++;
        echo "Диск $n с стержня $a на стержень $b <br />";
        towerOfHanoi($n - 1, $c, $b, $a);
    }
}

if (isset($_POST['submit'])) {
    $n = $_POST['num'];
    towerOfHanoi($n, 'A', 'C', 'B');
    echo "Количество ходов:" . $step;
}
?>
</body>
</html>