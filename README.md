# Tower Of Hanoi
А simple algorithm of Tower Of Hanoi in PHP

![Tower Of Hanoi Salar Bahador](https://i.pinimg.com/originals/4d/53/d6/4d53d6286367d32f832202e3095ab2e3.jpg)

## Introduction

Tower of Hanoi game is a puzzle invented by French mathematician Édouard Lucas in 1883.

**History of Tower of Hanoi**

There is a story about an ancient temple in India (Some say it’s in Vietnam – hence the name Hanoi)
has a large room with three towers surrounded by 64 golden disks (1.844674407371E+19 days).

These disks are continuously moved by priests in the temple. According to a prophecy,
when the last move of the puzzle is completed the world will end.
These priests acting on the prophecy, follow the immutable rule by Lord Brahma
of moving these disk one at a time.Hence this puzzle is often called Tower of Brahma puzzle.
Tower of Hanoi is one of the classic problems to look at if you want to learn **recursion**.
It is good to understand how **recursive** solutions are arrived at and how parameters for this recursion are implemented.

**What is the game of Tower of Hanoi?**

Tower of Hanoi consists of three pegs or towers with n disks placed one over the other.

The objective of the puzzle is to move the stack to another peg following these simple rules.

1) Only one disk can be moved at a time.
2) No disk can be placed on top of the smaller disk.

**Here is the simple algorithm of tower of hanoi in PHP :**
```
function towerOfHanoi($diskCount, $a = 'A', $b = 'B', $c = 'C')
{
    if ($diskCount == 1) {
        echo "move {$a} to {$c}". "<br>";
    } else {
        towerOfHanoi($diskCount - 1, $a, $c, $b);
        towerOfHanoi(1, $a, $b, $c);
        towerOfHanoi($diskCount - 1, $b, $a, $c);
    }
}
```